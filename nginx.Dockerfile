FROM alpine:3
EXPOSE 80
RUN apk --no-cache add nginx 
COPY ./nginx.conf /etc/nginx/
CMD ["nginx", "-g", "daemon off;"]